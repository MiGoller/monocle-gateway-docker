FROM ubuntu:latest

# Build arguments ...
# The app's version information
ARG ARG_APP_VERSION 

# The channel or branch triggering the build.
ARG ARG_APP_CHANNEL

# The commit sha triggering the build.
ARG ARG_APP_COMMIT

# Build data
ARG ARG_BUILD_DATE

# Basic build-time metadata as defined at http://label-schema.org
LABEL org.label-schema.build-date=${ARG_BUILD_DATE} \
    org.label-schema.docker.dockerfile="/Dockerfile" \
    org.label-schema.license="GPLv3" \
    org.label-schema.name="MiGoller" \
    org.label-schema.vendor="MiGoller" \
    org.label-schema.version=${ARG_APP_VERSION} \
    org.label-schema.description="Monocle Gateway for Docker" \
    org.label-schema.url="https://gitlab.com/users/MiGoller/projects" \
    org.label-schema.vcs-ref=${ARG_APP_COMMIT} \
    org.label-schema.vcs-type="Git" \
    org.label-schema.vcs-url="https://gitlab.com/MiGoller/monocle-gateway-docker.git" \
    maintainer="MiGoller" \
    Author="MiGoller"

# Persist app-reladted build arguments
ENV APP_VERSION=$ARG_APP_VERSION \
    APP_CHANNEL=$ARG_APP_CHANNEL \
    APP_COMMIT=$ARG_APP_COMMIT \
    APP_BUILD_DATE=$ARG_BUILD_DATE

# Install required binaries
RUN apt-get update && \
    apt-get install -y wget tar

# Create additional directories for: Custom configuration, working directory, database directory, scripts
RUN mkdir -p \
    /opt/monocle \
    /etc/monocle

# Assign working directory
WORKDIR /opt/monocle

# Download and install Monocle Gateway
RUN wget https://files.monoclecam.com/monocle-gateway/linux/monocle-gateway-linux-x64-v0.0.4.tar.gz -O /opt/monocle/monocle-gateway.tar.gz && \
    tar -xvzf /opt/monocle/monocle-gateway.tar.gz && \
    rm -f /opt/monocle/monocle-gateway.tar.gz

# Volumes
VOLUME ["/etc/monocle"]

# Exposed ports
EXPOSE 443
EXPOSE 8554
EXPOSE 8555

# Start Monocle Gateway
CMD [ "/opt/monocle/monocle-gateway" ]

# Monocle Gateway for Docker

## Monocle?

>[Monocle](https://monoclecam.com/) is a gateway service that enables seamless integration of your private IP-based network cameras with smart home automation services such as Amazon Alexa®.

## Monocle Gateway? What's that?

>[Monocle Gateway](https://forum.monoclecam.com/category/6/monocle-gateway) is a small service that runs on your local network to coordinate communication, encryption and security between your Alexa enabled devices and local IP cameras. The Monocle Gateway is a replacement for the previous proxy workaround service and is fully integrated with the Monocle platform. 

Please have a look at the [official Monocle Gateway site](https://forum.monoclecam.com/category/6/monocle-gateway) for further details.

## Why do I need a Docker image?

shadeBlue supports the [Monocle Gateway](https://forum.monoclecam.com/category/6/monocle-gateway) on Microsoft Windows and Linux (x86, x64, ARMv7 32 bit and ARMv8 64 bit), but there is currently no Docker support.

So, why do you want to run additional virtual machines or Raspberry Pis, if you have already got a Docker host up and running, right?

## Requirements & Prerequisites

Right now I do provide Debian based x64 Docker images only.

### Networking

The [Monocle Gateway](https://forum.monoclecam.com/category/6/monocle-gateway) has got some requirements due to the networking configuration.

[Monocle](https://monoclecam.com/) requires a fixed IP address, statically or dynamically assigned, for the [Monocle Gateway](https://forum.monoclecam.com/category/6/monocle-gateway). Don't worry about that requirement. Docker containers with their own networks do work well.

The [Monocle Gateway](https://forum.monoclecam.com/category/6/monocle-gateway) creates a dynamically DNS record for the Docker host running the gateway, but will fail to register that DNS record. So, you'll have to create a corresponding DNS record on your firewall, router or DNS server. Your Amazon Alexa devices on your local network will communicate using the provided DNS record with the [Monocle Gateway](https://forum.monoclecam.com/category/6/monocle-gateway) Docker container.

Ensure your IP cameras and Amazon Alexa devices are allowed to access the gateway container on port 443 on your Docker host.

And that's right now the major caveeat: 
>No other service or application can be serving on port 443. Amazon requires that cameras only connect to port 443 using secure TLS encrypted communications. ... This port does not need to be exposed to the Internet through your router.

To circumvent that caveeat just add a new virtual IP to your Docker host and bind the [Monocle Gateway](https://forum.monoclecam.com/category/6/monocle-gateway) container to that IP only instead of 0.0.0.0 !

### Monocle API token

Generate and download your Monocle API token from your user account page in the Monocle web portal. See this topic for more details on how to generate your API token: [Monocle API Token](https://forum.monoclecam.com/topic/5/monocle-api-token)

Once you have downloaded your monocle API token file, copy it to the Monocle Gateway container's configuration directory. (i.e. `./config/monocle.token`).

## Dockerize Monocle Gateway

It's easy to dockerize the [Monocle Gateway](https://forum.monoclecam.com/category/6/monocle-gateway) if you keep the requirements and prerequisites in mind.

To dockerize the gateway create a directory for the Docker files and the config first.
```
$ mkdir ./monocle-gateway
$ mkdir ./monocle-gateway/config
```

### The docker-compose.file

Create a `docker-compose.yml` file inside the directory `./monocle-gateway` with content similar content.
```
version: '2'
services:
  gateway:
    image: migoller/monoclegatewaydocker
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
      - ./config:/etc/monocle
    ports:
      - "0.0.0.0:443:443"
```
Don't forget to replace the `0.0.0.0` with your desired IP to bind the gateway to!

### Your Monocle API Token

Save your private [Monocle API Token](https://forum.monoclecam.com/topic/5/monocle-api-token) to the directory `./monocle-gateway/config` and name the file `monocle.token`.

### Get the dynamic DNS record for the gateway

To get the dynamic DNS record for the gateway you will have to start the gateway container.
```
$ docker-compose up
```

The [Monocle Gateway](https://forum.monoclecam.com/category/6/monocle-gateway) stack will start and show log messages like these.
```
gateway_1  |
gateway_1  |  ******************************************************************
gateway_1  |  *             __  __  ___  _  _  ___   ___ _    ___              *
gateway_1  |  *            |  \/  |/ _ \| \| |/ _ \ / __| |  | __|             *
gateway_1  |  *            | |\/| | (_) | .` | (_) | (__| |__| _|              *
gateway_1  |  *            |_|  |_|\___/|_|\_|\___/ \___|____|___|             *
gateway_1  |  *                                                                *
gateway_1  |  ******************************************************************
gateway_1  |
gateway_1  |
gateway_1  | -------------------------------------------------
gateway_1  | MONOCLE RUNTIME ENVIRONMENT
gateway_1  | -------------------------------------------------
gateway_1  | VERSION   = 0.0.3
gateway_1  | OS/ARCH   = linux/x64
gateway_1  | PROCESS   = monocle-gateway (PID=1)
gateway_1  | TIMESTAMP = 2018-11-25T19:25:58.003Z
gateway_1  |
gateway_1  | -------------------------------------------------
gateway_1  | MONOCLE GATEWAY SERVICE         (Version: 0.0.3)
gateway_1  | -------------------------------------------------
gateway_1  | [Monocle Starting]
gateway_1  | [Monocle Connecting]
gateway_1  | [Monocle Started]
gateway_1  | [RTSP Server Starting]
gateway_1  | [RTSP Server Listening] 0.0.0.0:8555 (RTSP)
gateway_1  | [RTSP Server Listening] 0.0.0.0:443 (RTSP-TLS)
gateway_1  | [RTSP Proxy Started] (PID=14)
gateway_1  | [RTSP Server Listening] 0.0.0.0:8554 (PROXY)
gateway_1  | [RTSP Server Started]
gateway_1  | [Monocle Connected]
gateway_1  | [RTSP Server Registered]
gateway_1  |
gateway_1  | -------------------------------------------------
gateway_1  | MONOCLE RTSP SERVICE - INITIALIZED
gateway_1  | -------------------------------------------------
gateway_1  | FQDN = f55a71be-8cad-410b-9b85-db85f9f3de9b.mproxy.io
gateway_1  | HOST = 172.18.0.2
gateway_1  | PORT = 443
gateway_1  | -------------------------------------------------
```

Look for the `FQDN` entry. In this example we look for this line.
```
gateway_1  | FQDN = f55a71be-8cad-410b-9b85-db85f9f3de9b.mproxy.io
```

Now you can set a corresponding DNS record in your firewall, router or DNS server. Create an A-record for the `FQDN`.

Don't forget to stop the stack.
```
$ docker-compose up
```

That's it. The gateway container or stack should be up and running.

## Set up Monocle camera feeds

You have to tag your Monocle Camera Feeds on your [Monocle Dashboard](https://portal.monoclecam.com/). 

You have to add the tags `@tunnel` or `@proxy` to your camera feeds. Follow the instructions on [Tagging Cameras for Monocle Gateway](https://forum.monoclecam.com/topic/6/tagging-cameras-for-monocle-gateway). I have to add the `@proxy` tag to my feeds to be able to access my IP cameras' feed from my Amazon Fire TV.

If you let Alexa show one of your IP camera feeds the gateway will show log messages like these if anythings works as expected.
```
gateway_1  | -------------------------------------------------
gateway_1  | INITIALIZE RTSP STREAM:  <Feed Name>
gateway_1  | -------------------------------------------------
gateway_1  |  - NAME  : <Feed Name>
gateway_1  |  - LABEL : PRIMARY
gateway_1  |  - URL   : <RTSP-URL>
gateway_1  |  - UUID  : STREAM:6e788b66-25a5-4193-9e7f-fa04809c0d61
gateway_1  |  - SESS  : e7861d0d-d3c4-4df3-8696-435e5e57ed8b
gateway_1  |  - MODIF : Sat Nov 24 2018 11:49:48 GMT+0100 (CET)
gateway_1  |  - TAGS  : @proxy
gateway_1  | -------------------------------------------------
gateway_1  |
gateway_1  | 2018-11-25T21:05:44.382Z [INFO]  [RTSP PROXY] REGISTERING STREAM [<Feed Name>/PRIMARY]; (STREAM:6e788b66-25a5-4193-9e7f-fa04809c0d61)
gateway_1  | 2018-11-25T21:05:45.981Z [DEBUG] <RTSP-PROXY> [REQUEST]  --> [REGISTER] <RTSP-URL>
...
gateway_1  | 2018-11-25T21:05:47.424Z [INFO]  [...] RTSP ENDPOINT PLAYING MEDIA STREAM: <Feed Name> (STREAM:6e788b66-25a5-4193-9e7f-fa04809c0d61)
gateway_1  | 2018-11-25T21:05:47.424Z [INFO]  [...] RTSP CLIENT PLAYING MEDIA STREAM: <Feed Name> (STREAM:6e788b66-25a5-4193-9e7f-fa04809c0d61)
```

Good luck.

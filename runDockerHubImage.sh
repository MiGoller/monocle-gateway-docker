#!/bin/bash

trap cleanup 1 2 3 6

cleanup()
{
    echo "Docker-Compose ... cleaning up."
    docker-compose down
    echo "Docker-Compose ... quitting."
    exit 1
}

set -e

if [ ! -d ./config ]
then
    mkdir -p config
    chmod -R 777 config
fi

if [ -z ${1+x} ]; then
    echo "Usage: ./runDockerHubImage.sh [debian]"
else
    docker-compose -f ./docker-compose.DockerHub.${1}.yml up
fi
